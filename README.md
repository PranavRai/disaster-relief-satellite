# Disaster Relief Satellite

We present our code for the [XView 2 Challenge](https://xview2.org/) - which offers
pre-disaster and pos-disaster satellite images. We were tasked
with 1) Segmenting/Detecting buildings, and, 2) Assessing the
damage sustained.

We use modififed bits of the official [Official Baseline](https://github.com/DIUx-xView/xview2-baseline),
and develop our UNet (on ResNet34) model for Semantic Segmentation,
as well as ResNet34 for Classification. Due to GPU memory issues on our
16GB GPUs (:disappointed:), we crop the images to facilitate large batch sizes for
training, and this helps us perform (somewhat) better than the baseline's
UNEt model.

The following figure gives a graphical overview of [UNets](https://arxiv.org/abs/1505.04597)
![UNet Explained](https://miro.medium.com/max/902/1*O2NbipwBOdTMtj7ThBNTPQ.png)


Our **presentation** on this challenge can be found in [reports/Big Data Science - Final.pptx](reports/Big Data Science - Final.pptx).
Our pre-trained models will be downloaded automatically when our scripts are run.
Should you wish to train them yourself, we offer ipynb and py files for the same. Our tasks
and points of access are summarised below:

| Task | Interactive |File |
| ---- | --- | --- |
| **Inference** | Yes | [notebooks/final_view.ipynb](notebooks/final_view.ipynb) |
| **Semantic Segmentation** | Yes | [notebooks/segmentation_model.ipynb](notebooks/segmentation_model.ipynb) |
| **Classification** | Yes | [notebooks/classification.ipynb](notebooks/classification.ipynb) |
| **Data Visualisation** | Yes | [notebooks/visualization.ipynb.ipynb](notebooks/visualization.ipynb) |
| **Semantic Segmentation** | No | [src/Model/semantic_segmentation_model.py](src/Model/semantic_segmentation_model.py) |
| **Classification** | No | [src/Model/classification_model.py](src/Model/classification_model.py) |




# Instructions
### Preliminary
* [Download dataset](https://xview2.org/download-links) and place it in data/raw/ (@Florian: login details on slack -> disaster-channel)
* **To clone the submodule** (official baseline) as well as this project:
```
git clone --recurse-submodules <url>
```
* export PYTHONPATH so src is set as project root
```
export PYTHONPATH=src/:$PYTHONPATH
```
* Set up a virtual environment on python 3.7 using
```
python3.7 -m venv <env_name>
```
* Activate the environment, and install packages as follows:
```
pip install -r requirements.txt
```

### Data Processing
Most parameters, such as sample size, can be modified via command
line arguments

* Create samples from row data (default 1000 samples)
```
python src/generate_sample.py
```
* Create masks from labels
```
python src/utilities/mask_polygons.py
```
* Create cropped images and masks from samples
```
python src/utilities/crop_image.py
```
* Create polygons and csv file for classfication
```
python src/utilities/process_data_classification.py
```
-------------------------------------------------------
Now you can :
- Get predictions and scores from the pre-trained model files in the ipynb file# Training with python files, or,
- Train your own models on Notebooks or python files as follows:
```
python src/Model/train_model.py --model <model_num>
    <model_num>=0 for Segmentation model
    <model_num>=1 for Classification model
    <model_num>=2 for both models
```
* Scoring can be performed via
```
python src/utilities/scoring.py
```

# Official Baseline Instructions

Note: The official Baseline code requires GPU (due to imports of GPU specific packages eve when not required)

In case you wish to play around with the Official Baseline,
we once again refer you to [their repository](https://github.com/DIUx-xView/xview2-baseline)

We, however, have prepared ease-of-life scripts for the same under scripts/:
* run_preprocess.sh -> Prepares env, splits data, creates polygons
* run_localise.sh -> Trains localisation baseline model
* run_preprocess_damage.sh -> Makes directories, preprocesses for damage baseline
* run_damage.sh -> Trains damage classifier baseline model
* infer.sh -> Evaluation of a single image

# Useful Links

* [Official Baseline](https://github.com/DIUx-xView/xview2-baseline)
* [Baseline's Evaluation](https://github.com/DIUx-xView/xview2-scoring)
* [XView2 Discussion Board](https://discordapp.com/channels/624633738512433152/624633738512433154)


# Literature

<!-- IOU metric: https://github.com/Zzh-tju/DIoU -->

#### Pretrained Models

* [Land Use - EUROStat](https://github.com/tchambon/DeepSentinel)
* [Land Use](https://github.com/SakhriHoussem/Image-Classification)

#### SpaceNet/Xview2 repositories
* Xview 2 [link(almost standard)](https://github.com/mattdutson/xview2) [link](https://github.com/lucaskawazoi/lkk-xview2) [link](https://github.com/Jordan-johnson-OSU/xview2)
* Spacenet v2 [link](https://github.com/SpaceNetChallenge/BuildingDetectors_Round2)
* Spacenet v1 [link](https://github.com/SpaceNetChallenge/BuildingDetectors/tree/master/wleite)

#### Datasets

Ideal for our use case:
* [SpaceNet: Multiple versions!](https://spacenet.ai/datasets/)
    * versions 1 and 2 deal with buildings, #4 with highly off-nadir images
    * SpaceNet baseline is based on data from Rio de Janeiro
* [Canadian Footprints](https://github.com/Microsoft/CanadianBuildingFootprints)

Datasets (Buildings)
* [INRIA Aerial](https://project.inria.fr/aerialimagelabeling/)
* [Open AI Tanzania](https://competitions.codalab.org/competitions/20100#learn_the_details)
* [Open Cities](https://www.drivendata.org/competitions/60/building-segmentation-disaster-resilience/page/150/)
* [NWPU VHR-10](http://www.escience.cn/people/gongcheng/NWPU-VHR-10.html)
* Aerial Imagery Object Identification Dataset
* (Urban) TorontoCity

Datasets (Buildings + Others)
* [DOTA](https://captain-whu.github.io/DOTA/index.html)
* [Dstl](https://www.kaggle.com/c/dstl-satellite-imagery-feature-detection/data)

Datasets (other)
* [Land Use; AFCMOMA](http://www.lmars.whu.edu.cn/prof_web/zhongyanfei/e-code.html)
* Land Use; EUROStat
* Land Use; [UC Merced](http://weegee.vision.ucmerced.edu/datasets/landuse.html)  

## References

* Sudre, Carole H., et al. "Generalised dice overlap as a deep learning loss function for highly unbalanced segmentations." Deep learning in medical image analysis and multimodal learning for clinical decision support. Springer, Cham, 2017. 240-248.
* Lin, Tsung-Yi, et al. "Focal loss for dense object detection." Proceedings of the IEEE international conference on computer vision. 2017.
* Ronneberger, Olaf, Philipp Fischer, and Thomas Brox. "U-net: Convolutional networks for biomedical image segmentation." International Conference on Medical image computing and computer-assisted intervention. Springer, Cham, 2015.
* He, Kaiming, et al. "Deep residual learning for image recognition." Proceedings of the IEEE conference on computer vision and pattern recognition. 2016.
* Lam, Darius, et al. "xview: Objects in context in overhead imagery." arXiv preprint arXiv:1802.07856 (2018).
* Van Etten, Adam, Dave Lindenbaum, and Todd M. Bacastow. "Spacenet: A remote sensing dataset and challenge series." arXiv preprint arXiv:1807.01232 (2018).
* Wurm, Michael, et al. "Semantic segmentation of slums in satellite images using transfer learning on fully convolutional neural networks." ISPRS journal of photogrammetry and remote sensing 150 (2019): 59-69.
* Long, Jonathan, Evan Shelhamer, and Trevor Darrell. "Fully convolutional networks for semantic segmentation." Proceedings of the IEEE conference on computer vision and pattern recognition. 2015.
* Noh, Hyeonwoo, Seunghoon Hong, and Bohyung Han. "Learning deconvolution network for semantic segmentation." Proceedings of the IEEE international conference on computer vision. 2015.


<!-- ### Edits -->

<!-- * Baseline code requires GPU -->
<!-- * Fixed xview2-baseline code: data_finalize.sh line 92 -->
<!-- * Fixed xview2-baseline code: data_finalize.sh added line 103 -->
<!-- * 2-3 steps changes in baseline-algorithm workflow, e.g. deleting spacenet_gt folder -->
<!-- * log_dir needs to be changed for tensorboard in baseline/models/damage_classification.py -->
<!-- * Python version needs to be fixed to 3.6, for tensorflow-gpu to be installed -->

