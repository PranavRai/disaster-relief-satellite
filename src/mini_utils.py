from google_drive_downloader import GoogleDriveDownloader as gdd
import wget


def download_models(path, name):
    hash_name_id = {"segmentation_res34.pkl": "1oy6-rSyRL8LiLflTKZnltp-dt_BBRJKy",
                    "classification_res34.pkl": "12ned6EulLnAgx9JfmdTX3xJHGq9U7hMg",
                    "downloaded_full_baseline/classification.hdf5":
                        "https://github.com/DIUx-xView/xview2-baseline/releases/download/v1.0/classification.hdf5",
                    "downloaded_full_baseline/localization.h5":
                        "https://github.com/DIUx-xView/xview2-baseline/releases/download/v1.0/localization.h5"}
    # print(f'Enter google-drive file id ({name}):')
    # file_id = input()

    if name == "segmentation_res34.pkl" or name == "classification_res34.pkl":
        gdd.download_file_from_google_drive(file_id=hash_name_id[name], dest_path=path,
                                            unzip=False, showsize=True, overwrite=True)
    else:
        wget.download(hash_name_id[name], out=path)


def create_blank_file(destination: str):
    """
    Creating a blank file (mostly for testing purposes)
    :param destination: string
    :return:
    """
    with open(destination, "w") as w:
        w.write("\n")
