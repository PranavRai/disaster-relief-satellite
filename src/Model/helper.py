from fastai.vision import open_mask, nn, SegmentationLabelList, SegmentationItemList, Tensor, Rank0Tensor, F
import torch


# subclassing SegmentationLabelList to set open_mask(fn, div=True, convert_mode='RGB') for 3 channel target masks


class SegLabelListCustom(SegmentationLabelList):
    def open(self, fn):
        return open_mask(fn, div=False, convert_mode='RGB')


class SegItemListCustom(SegmentationItemList):
    _label_cls = SegLabelListCustom


def dice_loss(input_, target):
    smooth = 1.
    input_ = torch.sigmoid(input_)
    iflat = input_.contiguous().view(-1).float()
    tflat = target.contiguous().view(-1).float()
    intersection = (iflat * tflat).sum()
    # print((iflat * tflat).sum() + smooth)
    return 1 - ((2. * intersection + smooth) / ((iflat + tflat).sum() + smooth))


class FocalLoss(nn.Module):
    def __init__(self, alpha=1, gamma=2, reduction='mean'):
        super().__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.reduction = reduction
        self.count = 0

    def forward(self, inputs, targets):
        eps = 1e-8
        inputs = torch.clamp(inputs, eps, 1. - eps)
        targets = torch.clamp(targets, eps, 1. - eps)
        bce_loss = F.binary_cross_entropy_with_logits(inputs, targets.float(), reduction='none')
        pt = torch.exp(-1 * bce_loss)
        f_loss = self.alpha * (1 - pt) ** self.gamma * bce_loss
        # f_loss = torch.clamp(f_loss, min=-0.01, max=0.01)
        # print(f_loss, f_loss.mean())
        # print("\n\n")
        # self.count+=1
        # if self.count>3:
        #     exit()

        if self.reduction == 'mean':
            return f_loss.mean()
        elif self.reduction == 'sum':
            return f_loss.sum()
        else:
            return f_loss


class DiceLoss(nn.Module):
    def __init__(self, reduction='mean'):
        super().__init__()
        self.reduction = reduction

    def forward(self, input_, target):
        loss = dice_loss(input_, target)

        if self.reduction == 'mean':
            return loss.mean()
        elif self.reduction == 'sum':
            return loss.sum()
        else:
            return loss


class MultiChComboLoss(nn.Module):
    def __init__(self, reduction='mean', loss_funcs=[FocalLoss(), DiceLoss()], loss_wts=[1, 1], ch_wts=[1, 1, 1]):
        super().__init__()
        self.reduction = reduction
        self.loss_funcs = loss_funcs
        self.loss_wts = loss_wts
        self.ch_wts = ch_wts

    def forward(self, output, target):
        for loss_func in self.loss_funcs:
            loss_func.reduction = self.reduction
        loss = 0
        channels = output.shape[1]
        assert len(self.ch_wts) == channels
        assert len(self.loss_wts) == len(self.loss_funcs)
        for ch_wt, c in zip(self.ch_wts, range(channels)):
            ch_loss = 0
            for loss_wt, loss_func in zip(self.loss_wts, self.loss_funcs):
                ch_loss += loss_wt * loss_func(output[:, c, None], target[:, c, None])
            loss += ch_wt * ch_loss
        loss = loss / sum(self.ch_wts)
        # loss = torch.clamp(loss, min=-0.1, max=0.1)
        # print(loss)
        return loss


def acc_thresh_multich(input_: Tensor, target: Tensor, thresh: float = 0.5, sigmoid: bool = True,
                       one_ch: int = None) -> Rank0Tensor:
    """
    Compute accuracy when `y_pred` and `y_true` are the same size. C
    calculate metrics on one channel or on all 3 channels
    """

    #     pdb.set_trace()
    if sigmoid:
        input_ = input_.sigmoid()
    n = input_.shape[0]

    if one_ch is not None:
        input_ = input_[:, one_ch, None]
        target = target[:, one_ch, None]

    input_ = input_.view(n, -1)
    target = target.view(n, -1)
    return ((input_ > thresh) == target.byte()).float().mean()


def dice_multich(input_: Tensor, targets: Tensor, iou: bool = False, one_ch: int = None) -> Rank0Tensor:
    """Dice coefficient metric for binary target. If iou=True, returns iou metric, classic for segmentation problems."""
    #     pdb.set_trace()
    n = targets.shape[0]
    input_ = input_.sigmoid()

    if one_ch is not None:
        input_ = input_[:, one_ch, None]
        targets = targets[:, one_ch, None]

    input_ = (input_ > 0.5).view(n, -1).float()
    targets = targets.view(n, -1).float()

    intersect = (input_ * targets).sum().float()
    union = (input_ + targets).sum().float()
    if not iou:
        return 2. * intersect / union if union > 0 else union.new([1.]).squeeze()
    else:
        # print(union - intersect + 1.0)
        return intersect / (union - intersect + 1.0)
