from src import CROPS_PATH, SAMPLE_PATH, CLASSIFICATION_PATH
from src.Model import semantic_segmentation_model, classification_model
import argparse
import sys
from distutils.util import strtobool


def segmentation(cropping: bool):
    path = CROPS_PATH if cropping else SAMPLE_PATH
    semantic_segmentation_model.build(path)


def classification():
    path = CLASSIFICATION_PATH
    classification_model.build(path)


def parse_args(args):
    parser = argparse.ArgumentParser(description='Run Building Segmentation Training or Damage Classification Training')
    parser.add_argument('--model',
                        required=True,
                        metavar="0/1/2",
                        default=0,
                        help="Choose which model to be trained: "
                             " 0 for Segmentation model,"
                             " 1 for Classification model,"
                             " 2 for both models")
    parser.add_argument('--cropping',
                        required=False,
                        metavar='True/False',
                        default=True,
                        help="Train the Segmentation model with cropped tiles or origin-size images")

    args = parser.parse_args(args)
    cropping = args.cropping if bool(args.cropping) else strtobool(args.cropping)
    if int(args.model) not in [0, 1, 2]:
        raise ValueError("please enter correct number: 0 for Segmentation model, 1 for Classification model, "
                         " 2 for both models")

    if cropping not in [True, False]:
        raise TypeError("Please only select True/False")

    return args


def main():
    args = parse_args(sys.argv[1:])
    model = int(args.model)
    cropping = args.cropping if bool(args.cropping) else strtobool(args.cropping)
    if model == 0:
        segmentation(cropping)
    elif model == 1:
        classification()
    elif model == 2:
        segmentation(cropping)
        classification()


if __name__ == "__main__":
    main()
