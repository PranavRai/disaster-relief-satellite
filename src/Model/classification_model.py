from fastai.vision import models, get_transforms, imagenet_stats, ImageList, cnn_learner
from fastai.metrics import error_rate  # 1 - accuracy

from src import MODELS_PATH


def build(path: str, size: int = 128, bs: int = 40, lr: float = 1e-2):
    model_type, model_name = [(models.resnet18, "res18"), (models.resnet34, "res34"), (models.resnet50, "res50")][1]
    tfms = get_transforms(flip_vert=True, max_warp=0.1, max_rotate=20, max_zoom=2, max_lighting=0.3)

    data = (ImageList.from_csv(path, csv_name="train.csv", folder='polygons')
            .split_by_rand_pct()
            .label_from_df(cols='labels')
            .transform(tfms, size=size)
            .databunch(bs=bs)
            .normalize(imagenet_stats))

    learn = cnn_learner(data, model_type, metrics=error_rate)

    # Training with freezing the encoder
    print("Training with freezing the encoder")
    learn.fit_one_cycle(20, max_lr=lr)

    # Unfreeze the encoder
    print("Training with unfreezing the encoder")
    learn.unfreeze()
    learn.fit_one_cycle(20, max_lr=slice(3e-5, 3e-4))
    print("Finished training!")

    # Save model
    learn.export(f'{MODELS_PATH}/classification_{model_name}.pkl')
    print(f'classification_{model_name}.pkl has been saved in {MODELS_PATH}!')
