from fastai.vision import get_transforms, unet_learner, imagenet_stats, partial, models
from fastai.callbacks import SaveModelCallback
import numpy as np
from src.Model.helper import FocalLoss, DiceLoss, MultiChComboLoss, acc_thresh_multich, dice_multich, SegItemListCustom

from src import MODELS_PATH


def build(path: str, size: int = 256, bs: int = 40, lr: float = 1e-3):
    path_img = path + '/images/'
    path_mask = path + '/masks/'

    # the classes corresponding to each channel
    codes = np.array(['R', 'G', 'B'])
    model_type, model_name = [(models.resnet18, "res18"), (models.resnet34, "res34"), (models.resnet50, "res50")][1]

    # Mapping image path to mask path
    def get_y_fn(x):
        return path_mask + f'{x.stem + x.suffix}'

    # define image transforms for data augmentation and create databunch
    tfms = get_transforms(flip_vert=True, max_warp=0.1, max_rotate=20, max_zoom=2, max_lighting=0.3)

    data = (SegItemListCustom.from_folder(path_img)
            .split_by_rand_pct()
            .label_from_func(get_y_fn, classes=codes)
            .transform(tfms, size=size, tfm_y=True)
            .databunch(bs=bs, path=path)
            .normalize(imagenet_stats))

    # set up metrics to show mean metrics for all channels as well as the building-only metrics (channel 0)
    acc_ch0 = partial(acc_thresh_multich, one_ch=0)
    dice_ch0 = partial(dice_multich, one_ch=0)
    metrics = [acc_thresh_multich, dice_multich, acc_ch0, dice_ch0]

    # combo Focal + Dice loss with equal channel wts
    learn = unet_learner(data, model_type, model_dir=MODELS_PATH,
                         metrics=metrics,
                         loss_func=MultiChComboLoss(
                             reduction='mean',
                             loss_funcs=[FocalLoss(gamma=1, alpha=0.95), DiceLoss()],
                             loss_wts=[1, 1],
                             ch_wts=[1, 1, 1])
                         )

    # Training with freezing the encoder
    print("Training with freezing the encoder")
    learn.fit_one_cycle(20, max_lr=lr)

    # Unfreeze the encoder
    print("Training with unfreezing the encoder")
    learn.unfreeze()
    learn.fit_one_cycle(20, max_lr=slice(3e-5, 3e-4))
    print("Finished training!")

    # Save model
    learn.export(f'{MODELS_PATH}/segmentation_{model_name}.pkl')
    print(f'segmentation_{model_name}.pkl has been saved in {MODELS_PATH}!')
