import unittest
import os
import cv2
import numpy as np
import pandas as pd

# from src import DATA_PATH
from Model import train_model
from utilities.mask_polygons import save_one_mask
from utilities.crop_image import crop_image
# from utilities.damage_inference import create_generator


class TestModels(unittest.TestCase):
    def test_model_choice(self):
        self.assertRaises(ValueError, train_model.parse_args, ["--model", "3"])

    def test_model_cropping(self):
        self.assertRaises(TypeError, train_model.parse_args, ["--model", "2", "--cropping", "4"])

    def test_model_masks(self):
        self.assertRaises(FileNotFoundError, save_one_mask, [np.zeros((5, 5))], "foo_bar.json", "foo_bar.png")

    def test_model_cropping_tile_size(self):
        blank_image = np.zeros((1024, 1024, 3))
        cv2.imwrite("foo_bar.png", blank_image)
        self.assertRaises(ValueError, crop_image, "foo_bar.png", "foo_bar_1", (9999, 512), (256, 256))
        self.assertRaises(ValueError, crop_image, "foo_bar.png", "foo_bar_1", (256, -2), (256, 256))
        self.assertRaises(ValueError, crop_image, "foo_bar.png", "foo_bar_1", (256, 256), (4242, 512))
        self.assertRaises(ValueError, crop_image, "foo_bar.png", "foo_bar_1", (256, 256), (0, 512))
        os.remove("foo_bar.png")

    # Unnecessary
    # def test_damage_generator(self):
    #     # no_poly_json = '{"features": {"lng_lat": [], "xy": []}, "metadata": {"sensor": "WORLDVIEW03_VNIR", ' \
    #     #                '"provider_asset_type": "WORLDVIEW03_VNIR", "gsd": 1.3489, "capture_date":
    #     #                "2021-03-05T17:10:18.000Z", "off_nadir_angle": 97.933279, "pan_resolution": 0.46,
    #     #                "sun_azimuth": 350.80763000000002, "sun_elevation": 55.506645, "target_azimuth": 118.24727,
    #     #                "disaster": "foo_bar", "disaster_type": "volcano", "catalog_id": "10400121240",
    #     #                "original_width": 1024, "original_height": 1024, "width": 1024, "height": 1024,
    #     #                "id": "MjUxMzEyasd12", "img_name": "foo_bar.png"}}'
    #     self.assertRaises(KeyError, create_generator, pd.DataFrame(columns=["uuid"]), ".", "asxz.json")


if __name__ == '__main__':
    unittest.main()
