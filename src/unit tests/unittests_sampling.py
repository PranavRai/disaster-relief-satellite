import unittest
import os
import shutil

# import data
import generate_sample
import mini_utils
from src import ROOT_PATH, DATA_PATH, SAMPLE_PATH


class TestSampling(unittest.TestCase):
    def test_gen_sample(self):
        # Note: This test is configured for running on the git cloud, where the data directory is not present
        # It is not designed to be run locally with data present. It can be tested locally by manually moving data
        if not os.path.exists(DATA_PATH + "/images/"):
            for folder in ["/images/", "/labels/", "/masks/"]:
                os.makedirs(DATA_PATH + folder)
                # os.makedirs(SAMPLE_PATH + folder)
            mini_utils.create_blank_file(DATA_PATH + "/labels/dummy_pre_disaster.json")
            mini_utils.create_blank_file(DATA_PATH + "/labels/dummy_post_disaster.json")
            mini_utils.create_blank_file(DATA_PATH + "/images/dummy_pre_disaster.png")
            mini_utils.create_blank_file(DATA_PATH + "/images/dummy_post_disaster.png")

            generate_sample.generate(100)
            self.assertTrue(os.path.isfile(SAMPLE_PATH + "/labels/dummy_pre_disaster.json"))
            self.assertTrue(os.path.isfile(SAMPLE_PATH + "/labels/dummy_post_disaster.json"))
            self.assertTrue(os.path.isfile(SAMPLE_PATH + "/images/dummy_pre_disaster.png"))
            self.assertTrue(os.path.isfile(SAMPLE_PATH + "/images/dummy_post_disaster.png"))

            shutil.rmtree(ROOT_PATH + "/data")

    def test_gen_sample_clean(self):
        temp_path = "testing_123/this_is_a_test/I_repeat/"
        self.assertRaises(FileNotFoundError, generate_sample.clean_previous, temp_path)

        os.makedirs(temp_path)
        mini_utils.create_blank_file(temp_path + "test.txt")

        generate_sample.clean_previous(temp_path)
        self.assertFalse(os.path.isfile(temp_path + "test.txt"))
        shutil.rmtree("testing_123")

    # LEGACY Unit tests for code that is now refactored out
    # def test_data_read(self):
    #     pairs = data.read_raw(sample=True)
    #     self.assertIsInstance(pairs, list)
    #     self.assertIsInstance(pairs[0], tuple)
    #     self.assertIsInstance(pairs[0][0], str)
    #     self.assertIsInstance(pairs[0][1], str)
    #
    #     pairs = data.read_raw(sample=False)
    #     self.assertIsInstance(pairs, list)
    #     self.assertIsInstance(pairs[0], tuple)
    #     self.assertIsInstance(pairs[0][0], str)
    #     self.assertIsInstance(pairs[0][1], str)
    #
    # def test_data_write(self):
    #     data.read_raw(sample=True)
    #     sample_check_loc = SAMPLE_PATH + "/train/"
    #     for check_loc in [sample_check_loc + "images/", sample_check_loc + "labels/"]:
    #         self.assertTrue(os.path.exists(check_loc))
    #         self.assertGreater(len(os.listdir(check_loc)), 0)


if __name__ == '__main__':
    unittest.main()
