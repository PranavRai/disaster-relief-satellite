import unittest
import numpy as np
from torch import tensor
from Model.helper import dice_loss, acc_thresh_multich, dice_multich


class TestLosses(unittest.TestCase):
    @classmethod
    def setUpClass(TestLosses):
        TestLosses.input = np.ones((128, 128, 3))
        TestLosses.target = np.ones((128, 128, 3))

    # Test dice_loss()
    def test_dice_init(self):
        result = dice_loss(tensor(self.input), tensor(self.target))
        self.assertEqual(float('%.3f' % result), 0.155)

    def test_dice_type_error(self):
        with self.assertRaises(TypeError):
            dice_loss(self.input, self.target)

    def test_dice_dimension_match(self):
        input_match = np.ones((128, 128))
        with self.assertRaises(RuntimeError):
            dice_loss(tensor(input_match), tensor(self.target))

    # Test acc_thresh_multich_init()
    def test_acc_thresh_multich_init(self):
        result = acc_thresh_multich(tensor(self.input), tensor(self.target))
        self.assertEqual(float(result), 1.)

    def test_dice_attribute_error(self):
        with self.assertRaises(AttributeError):
            acc_thresh_multich(self.input, self.target)

    def test_acc_dimension_match(self):
        input_match = np.ones((128, 128))
        with self.assertRaises(RuntimeError):
            acc_thresh_multich(tensor(input_match), tensor(self.target))

    def test_acc_thresh_multich_one_channel(self):
        target_zeros = tensor(np.zeros((128, 128, 3)))
        result = acc_thresh_multich(tensor(self.input), tensor(target_zeros), one_ch=0)
        self.assertEqual(float(result), 0.)

    # Test dice_multich()
    def test_dice_multich_init(self):
        result = dice_multich(tensor(self.input), tensor(self.target))
        self.assertEqual(float(result), 1.)

    def test_dice_multich_attribute_error(self):
        with self.assertRaises(AttributeError):
            dice_multich(self.input, self.target)

    def test_dice_multich_match(self):
        input_match = np.ones((128, 128))
        with self.assertRaises(RuntimeError):
            dice_multich(tensor(input_match), tensor(self.target))

    def test_dice_multich_one_channel(self):
        target_zeros = tensor(np.zeros((128, 128, 3)))
        result = dice_multich(tensor(self.input), tensor(target_zeros), one_ch=0)
        self.assertEqual(float(result), 0.)

    def test_dice_multich_IoU(self):
        input_zeros = tensor(np.zeros((128, 128, 3)))
        result = dice_multich(tensor(input_zeros), tensor(self.target), iou=True)
        self.assertEqual(float(result), 0.)


if __name__ == "__main__":
    unittest.main()
