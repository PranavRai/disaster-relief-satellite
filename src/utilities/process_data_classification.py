from PIL import Image
import numpy as np
import pandas as pd
from tqdm import tqdm
import os
import shutil
import json
import cv2
from collections import defaultdict
import shapely.wkt
import shapely

from src import SAMPLE_PATH, CLASSIFICATION_PATH

# Configurations
damage_intensity_encoding = defaultdict(lambda: 0)
damage_intensity_encoding['destroyed'] = 3
damage_intensity_encoding['major-damage'] = 2
damage_intensity_encoding['minor-damage'] = 1
damage_intensity_encoding['no-damage'] = 0


def process_img(img_array, polygon_pts, scale_pct):
    """Process Raw Data into

            Args:
                img_array (numpy array): numpy representation of image.
                polygon_pts (array): corners of the building polygon.

            Returns:
                numpy array: .

    """

    height, width, _ = img_array.shape

    xcoords = polygon_pts[:, 0]
    ycoords = polygon_pts[:, 1]
    xmin, xmax = np.min(xcoords), np.max(xcoords)
    ymin, ymax = np.min(ycoords), np.max(ycoords)

    xdiff = xmax - xmin
    ydiff = ymax - ymin

    # Extend image by scale percentage
    xmin = max(int(xmin - (xdiff * scale_pct)), 0)
    xmax = min(int(xmax + (xdiff * scale_pct)), width)
    ymin = max(int(ymin - (ydiff * scale_pct)), 0)
    ymax = min(int(ymax + (ydiff * scale_pct)), height)

    return img_array[ymin:ymax, xmin:xmax, :]


def process_data(input_path, output_path, output_csv_path):
    """Process Raw Data into polygons and csv

        Args:
            input_path (string): Path to dataset
            output_path (string): Path to store polygons
            output_csv_path (string): Path to store csv file

        Returns:
            x_data: A list of numpy arrays representing the images for training
            y_data: A list of labels for damage represented in matrix form

    """
    x_data = []
    y_data = []

    img_list = [input_path + x for x in os.listdir(input_path)]

    for img_path in tqdm(img_list):

        img_obj = Image.open(img_path)
        img_array = np.array(img_obj)

        # Get corresponding label for the current image
        label_path = img_path.replace('png', 'json').replace('images', 'labels')
        label_file = open(label_path)
        label_data = json.load(label_file)

        for feat in label_data['features']['xy']:

            # only images post-disaster will have damage type
            try:
                damage_type = feat['properties']['subtype']
            except KeyError:  # pre-disaster damage is default no-damage
                damage_type = "no-damage"
                continue

            poly_uuid = feat['properties']['uid'] + ".png"

            y_data.append(damage_intensity_encoding[damage_type])

            polygon_geom = shapely.wkt.loads(feat['wkt'])
            polygon_pts = np.array(list(polygon_geom.exterior.coords))
            poly_img = process_img(img_array, polygon_pts, 0.8)
            cv2.imwrite(output_path + "/" + poly_uuid, poly_img)
            x_data.append(poly_uuid)

    output_train_csv_path = os.path.join(output_csv_path, "train.csv")

    data_array = {'uuid': x_data, 'labels': y_data}
    df = pd.DataFrame(data=data_array, index=x_data)

    df.to_csv(output_train_csv_path)


if __name__ == "__main__":
    path_img = SAMPLE_PATH + '/images/'
    output_poly = CLASSIFICATION_PATH + "/polygons/"
    output_csv = CLASSIFICATION_PATH

    # clear the polygon folder
    shutil.rmtree(output_poly)
    os.mkdir(output_poly)

    # data processing for classification model
    print("Processing Data")
    process_data(path_img, output_poly, output_csv)
    print("Finished Processing Data")
