from pathlib import Path
import numpy as np
import skimage.io
from sklearn.metrics import f1_score
import pandas as pd

from random import sample
import cv2
from tqdm import tqdm

# Import Loss Functions
from Model.helper import *
from get_prediction import get_mask, get_mask_baseline, get_classes_bs, get_classes
from src import CROPS_PATH, SAMPLE_PATH, CLASSIFICATION_PATH


def dice_coef(y_true, y_pred, smooth=1):
    intersection = np.sum(y_true * y_pred, axis=(1, 2, 3))
    union = np.sum(y_true, axis=(1, 2, 3)) + np.sum(y_pred, axis=(1, 2, 3))
    dice = np.mean((2. * intersection + smooth) / (union + smooth), axis=0)
    return dice


def iou_coef(y_true, y_pred, smooth=1):
    intersection = np.sum(y_true * y_pred, axis=(1, 2, 3))
    union = np.sum(y_true, axis=(1, 2, 3)) + np.sum(y_pred, axis=(1, 2, 3)) - intersection
    iou = np.mean((intersection + smooth) / (union + smooth), axis=0)
    return iou


def scoring_segmentation(samples_num, model_name='segmentation_res34.pkl', cropping=False):
    path = CROPS_PATH if cropping else SAMPLE_PATH
    path_img = Path(path + '/images')
    path_mask = Path(path + '/masks')

    def get_mask_path(img):
        return path_mask / f'{img.stem + img.suffix}'

    true_masks = []
    our_masks = []
    bs_masks = []

    # randomly choose n samples from dataset for scoring
    max_num = len(path_img.ls())
    samples_num = max_num if samples_num > max_num else samples_num
    samples = sample(path_img.ls(), samples_num)
    print("Calculating the score!")
    for x in tqdm(samples):
        mask = skimage.io.imread(get_mask_path(x))
        image = skimage.io.imread(x)

        if not cropping:
            image = cv2.resize(image, (256, 256), interpolation=cv2.INTER_AREA)
            mask = cv2.resize(mask, (256, 256), interpolation=cv2.INTER_AREA)

        our_prediction = get_mask(image, model_name)
        bs_prediction = get_mask_baseline(image)

        true_masks.append(mask)
        our_masks.append(our_prediction)
        bs_masks.append(bs_prediction)

    true_masks = (np.array(true_masks) * 255).astype('uint8')
    our_masks = np.array(our_masks)
    bs_masks = np.array(bs_masks)

    print("Segmentation Model:")
    print("Dice:")
    print("our model: " + str(dice_coef(true_masks, our_masks)))
    print("baseline model: " + str(dice_coef(true_masks, bs_masks)))
    print("\n")
    print("IoU:")
    print("our model: " + str(iou_coef(true_masks, our_masks)))
    print("baseline model: " + str(iou_coef(true_masks, bs_masks)))
    print("\n")


def scoring_classification(samples_num, model_name='classification_res34.pkl'):
    print("Calculating the score!")
    csv_path = CLASSIFICATION_PATH + "/train.csv"
    polygons_path = CLASSIFICATION_PATH + "/polygons/"

    df = pd.read_csv(csv_path, engine='python')
    max_num = len(df.index)
    samples_num = max_num if samples_num > max_num else samples_num
    # Get true classes
    true_classes = []
    for x in df["labels"][:samples_num]:
        true_classes.append(x)
    # Get prediction classes from baseline model
    bs_classes = get_classes_bs(polygons_path, csv_path, samples_num)
    # Get prediction classes from our model
    our_classes = get_classes(polygons_path, csv_path, samples_num, model_name)

    print("Classification Model:")
    print("F1:")
    print("our model: " + str(f1_score(true_classes, our_classes, average='micro')))
    print("baseline model: " + str(f1_score(true_classes, bs_classes, average='micro')))
    print("\n")


def main():
    scoring_segmentation(100)
    scoring_classification(1000)


if __name__ == "__main__":
    main()
