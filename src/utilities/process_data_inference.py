from PIL import Image
import time
import numpy as np
import pandas as pd
from tqdm import tqdm
import os
import math
import random
import argparse
import logging
import json
import cv2
import datetime

import shapely.wkt
import shapely
from shapely.geometry import Polygon
from collections import defaultdict
from imantics import Polygons, Mask


def process_img(img_array: np.array,
                polygon_pts: np.array,
                scale_pct: float) -> np.array:
    """
    Extract polygons from raw data
    :param img_array: numpy representation of image.
    :param polygon_pts: corners of the building polygon.
    :param scale_pct: Extend Image by scale percentage
    :return: extracted polygon image from img_array.
    """

    height, width, _ = img_array.shape

    # Find the four corners of the polygon
    xcoords = polygon_pts[:, 0]
    ycoords = polygon_pts[:, 1]
    xmin, xmax = np.min(xcoords), np.max(xcoords)
    ymin, ymax = np.min(ycoords), np.max(ycoords)

    xdiff = xmax - xmin
    ydiff = ymax - ymin

    # Extend image by scale percentage
    xmin = max(int(xmin - (xdiff * scale_pct)), 0)
    xmax = min(int(xmax + (xdiff * scale_pct)), width)
    ymin = max(int(ymin - (ydiff * scale_pct)), 0)
    ymax = min(int(ymax + (ydiff * scale_pct)), height)

    return img_array[ymin:ymax, xmin:xmax, :]


def process_img_poly(img_obj: Image.Image,
                     label_path: str,
                     output_dir: str,
                     output_csv: str) -> None:
    """
    Extract (through process_img() ) polygons from image, and save to csv
    :param img_obj: Image.Image representation of image
    :param label_path: path to json file with polygon information
    :param output_dir: path where each polygon will be saved
    :param output_csv: path to csv containing list of polygons for this image
    :return:
    """
    x_data = []
    # img_obj = Image.open(img_path)

    img_array = np.array(img_obj)
    # Applies histogram equalization to image
    # clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    # img_array = clahe.apply(img_array_pre)

    # Get corresponding label for the current image
    label_file = open(label_path)
    label_data = json.load(label_file)

    # Find all polygons in a given image
    for feat in label_data['features']['xy']:
        poly_uuid = feat['properties']['uid'] + ".png"

        # Extract the polygon from the points given
        polygon_geom = shapely.wkt.loads(feat['wkt'])
        polygon_pts = np.array(list(polygon_geom.exterior.coords))
        poly_img = process_img(img_array, polygon_pts, 0.8)

        # Write out the polygon in its own image
        cv2.imwrite(output_dir + "/" + poly_uuid, poly_img)
        x_data.append(poly_uuid)

    data_array = {'uuid': x_data}
    df = pd.DataFrame(data=data_array)
    df.to_csv(output_csv)
