from PIL import Image
import time
import numpy as np
import pandas as pd
from tqdm import tqdm
import os
import math
import random
import argparse
import logging
import json
from sys import exit
import cv2
import datetime
import shapely.wkt
import shapely
from shapely.geometry import Polygon
from collections import defaultdict
import tensorflow as tf
import keras
from keras.callbacks import TensorBoard

import sys
from src import BASELINE_PATH
sys.path.append(BASELINE_PATH + "/model")

from model import generate_xBD_baseline_model, ordinal_loss
from keras import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense

# Configurations
# NUM_WORKERS = 4
# NUM_CLASSES = 4
# NUM_EPOCHS = 120

LOG_DIR = '/tmp/inference/classification_log_' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")


def create_generator(test_df: pd.DataFrame,
                     test_dir: str,
                     output_json_path: str,
                     batch_size: int = 64) -> keras.preprocessing.image.DataFrameIterator:
    """
    Creates data generator for validation set
    :param test_df: parent dataset csv
    :param test_dir: parent dataset directory
    :param output_json_path: Json file with predictions
    :return: Generator over data
    """

    gen = keras.preprocessing.image.ImageDataGenerator(rescale=1.4)

    try:
        gen_flow = gen.flow_from_dataframe(dataframe=test_df,
                                           directory=test_dir,
                                           x_col='uuid',
                                           batch_size=batch_size,
                                           shuffle=False,
                                           seed=123,
                                           class_mode=None,
                                           target_size=(128, 128))
    # No polys detected so write out a blank json
    except ValueError:
        blank = {}
        with open(output_json_path, 'w') as outfile:
            json.dump(blank, outfile)
        exit(0)

    return gen_flow


def run_inference(test_data: str,
                  test_csv: str,
                  model_weights: str,
                  output_json_path: str,
                  lr: float = 0.0001) -> None:
    """
    Runs inference on given test data and pretrained model
    :param test_data: Full path to the parent dataset directory
    :param test_csv: Full path to the parent dataset csv
    :param model_weights: Path to input weights
    :param output_json_path: Json file with predictions
    :return:
    """

    damage_intensity_encoding = {3: 'destroyed', 2: 'major-damage', 1: 'minor-damage', 0: 'no-damage'}

    model = generate_xBD_baseline_model()
    model.load_weights(model_weights)

    adam = keras.optimizers.Adam(lr=lr,
                                 beta_1=0.9,
                                 beta_2=0.999,
                                 epsilon=None,
                                 decay=0.0,
                                 amsgrad=False)

    model.compile(loss=ordinal_loss, optimizer=adam, metrics=['accuracy'])

    df = pd.read_csv(test_csv)

    test_gen = create_generator(df, test_data, output_json_path)
    test_gen.reset()
    samples = df["uuid"].count()

    # steps = np.ceil(samples / BATCH_SIZE)

    # tensorboard_callbacks = TensorBoard(log_dir=LOG_DIR, histogram_freq=1)

    predictions = model.predict_generator(generator=test_gen,
                                          # callbacks=[tensorboard_callbacks],
                                          verbose=1)

    predicted_indices = np.argmax(predictions, axis=1)
    predictions_json = dict()
    for i in range(samples):
        filename_raw = test_gen.filenames[i]
        filename = filename_raw.split(".")[0]
        predictions_json[filename] = damage_intensity_encoding[predicted_indices[i]]

    with open(output_json_path, 'w') as outfile:
        json.dump(predictions_json, outfile)
