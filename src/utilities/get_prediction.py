from fastai.vision import load_learner, Image, image2np, pil2tensor, open_image
import numpy as np
import pandas as pd
import cv2
import skimage.io

import sys
import os

from src import BASELINE_PATH, MODELS_PATH

sys.path.append(BASELINE_PATH + '/spacenet/src/models')
sys.path.append(BASELINE_PATH + '/model')
from segmentation_cpu import SegmentationModel as Model
from model import generate_xBD_baseline_model

# Import Loss Functions


def get_mask(img: Image, model_name='segmentation_res34.pkl') -> Image:
    segmentation_model = load_learner(path=MODELS_PATH, file=model_name)

    img = Image(pil2tensor(img[:, :, :3], np.float32).div_(255))
    segmented_img = segmentation_model.predict(img)
    segmented_img = image2np(segmented_img[2].sigmoid())
    segmented_img = (segmented_img > 0.9).astype('uint8')
    segmented_img = (segmented_img * 255).astype('uint8')

    return segmented_img


def get_mask_baseline(img: Image) -> Image:
    baseline_segmentation_model = MODELS_PATH + "/downloaded_full_baseline/localization.h5"
    mean = np.load(BASELINE_PATH + "/weights/mean.npy")
    bs_seg_model = Model(baseline_segmentation_model, mean)

    img = img[:, :, :3]
    score = bs_seg_model.apply_segmentation(img)
    segmented_img = (np.argmax(score, axis=0) == 1)
    channel = (segmented_img * 255).astype('uint8')
    img = np.stack((channel, channel, channel), axis=2)

    return img


def get_classes(polygons_path, csv_path, samples_num=100, model_name='classification_res34.pkl'):
    model = load_learner(path=MODELS_PATH, file=model_name)
    df = pd.read_csv(csv_path, engine='python')
    y = []
    for x in df["uuid"][:samples_num]:
        img = open_image(polygons_path + str(x))
        res = model.predict(img)[0]
        y.append(int(res))
    return np.array(y)


def get_classes_bs(polygons_path, csv_path, samples_num=100):
    classification_model = MODELS_PATH + "/downloaded_full_baseline/classification.hdf5"
    model = generate_xBD_baseline_model()
    model.load_weights(classification_model)

    df = pd.read_csv(csv_path, engine='python')
    y = []
    for x in df["uuid"][:samples_num]:
        img = skimage.io.imread(polygons_path + str(x))
        img = cv2.resize(img, (128, 128), interpolation=cv2.INTER_AREA)
        y.append(img)

    y = np.array(y)
    y_proba = model.predict(y)
    return y_proba.argmax(axis=-1)
