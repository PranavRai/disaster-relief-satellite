from src import SAMPLE_PATH, DATA_PATH
import json
from pathlib import Path

from cv2 import fillPoly, imwrite
import numpy as np
from shapely import wkt
from shapely.geometry import mapping
from skimage.io import imread
from tqdm import tqdm


def get_dimensions(file_path: str) -> tuple:
    """
    :param file_path: The path of the file
    :return: returns (width,height,channels)
    """
    # Open the image we are going to mask
    pil_img = imread(file_path)
    img = np.array(pil_img)
    w, h, c = img.shape
    return w, h, c


def mask_polygons_together(size: tuple, shapes: list) -> np.ndarray:
    """
    :param size: A tuple of the (width,height,channels)
    :param shapes: A list of points in the polygon from get_feature_info
    :returns: A numpy array with the polygons filled 255s where there's a building and 0 where not
    """
    # For each WKT polygon, read the WKT format and fill the polygon as an image

    mask_img = np.zeros(size, np.uint8)

    for u in shapes:
        blank = np.zeros(size, np.uint8)
        poly = shapes[u]
        fillPoly(blank, [poly], (1, 1, 1))
        mask_img += blank

    # Here we are taking the overlap (+=) and squashing it back to 0
    mask_img[mask_img > 1] = 0

    # Finally we are taking all 1s and making it pure white (255)
    mask_img[mask_img == 1] = 1

    return mask_img


def save_one_mask(masks: list, output_path: str, mask_file_name: str) -> bool:
    """
    :param masks: list of masked polygons from the mask_polygons_separately function
    :param output_path: path to save the masks
    :param mask_file_name: the file name the masks should have
    """
    # For each filled polygon, write the mask shape out to the file per image
    if (Path(output_path).is_dir()):
        mask_file_name = Path(output_path) / (mask_file_name + '.png')
        imwrite(str(mask_file_name), masks)
        return True
    else:
        raise FileNotFoundError("Directory doesn't exist ")


def read_json(json_path: str) -> dict:
    """
    :param json_path: path to load json from
    :returns: a python dictionary of json features
    """
    annotations = json.load(open(json_path))
    return annotations


def get_feature_info(feature: dict) -> dict:
    """
    :param feature: a python dictionary of json labels
    :returns: a list mapping of polygons contained in the image
    """
    # Getting each polygon points from the json file and adding it to a dictionary of uid:polygons

    props = {}

    for feat in feature['features']['xy']:
        feat_shape = wkt.loads(feat['wkt'])
        coords = list(mapping(feat_shape)['coordinates'][0])
        props[feat['properties']['uid']] = (np.array(coords, np.int32))

    return props


def mask_chips(json_path: str, images_directory: str, output_directory: str):
    """
    :param json_path: path to find multiple json files for the chips
    :param images_directory: path to the directory containing the images to be masked
    :param output_directory: path to the directory where masks are to be saved
    """
    # For each feature in the json we will create a separate mask
    # Getting all files in the directory provided for jsons
    print('Preparing masks of samples')
    jsons = [j.name for j in json_path.iterdir()]

    for j in tqdm([j for j in jsons if j.endswith('json')],
                  unit='poly',
                  leave=False):
        # Our chips start off in life as PNGs
        chip_image_id = j.replace(".json", ".png")
        mask_file = j.split('.')[0]

        # Loading the per chip json
        j_full_path = Path(json_path) / j
        chip_json = read_json(j_full_path)

        # Getting the full chip path, and loading the size dimensions
        chip_file = Path(images_directory) / chip_image_id
        chip_size = get_dimensions(chip_file)

        # Reading in the polygons from the json file
        polys = get_feature_info(chip_json)

        # if len(polys) > 0:
        masked_polys = mask_polygons_together(chip_size, polys)
        save_one_mask(masked_polys, output_directory, mask_file)


def main():
    # sample = False
    # path = SAMPLE_PATH if sample else DATA_PATH
    path = SAMPLE_PATH
    path_lbl = Path(path) / 'labels'
    path_img = Path(path) / 'images'
    path_mask = Path(path) / 'masks'
    print(path)

    mask_chips(json_path=path_lbl, images_directory=path_img, output_directory=path_mask)


if __name__ == "__main__":
    main()
