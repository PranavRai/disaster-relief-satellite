from pathlib import Path
import cv2
import math
import os
from tqdm import tqdm
from src import SAMPLE_PATH, CROPS_PATH


def get_files(base_dir: str) -> list:
    base_dir = Path(base_dir)
    files = [i for i in base_dir.iterdir()]
    return files


def crop_image(img_path: str, output: str, tile_size: tuple, offset: tuple):
    img = cv2.imread(str(img_path))
    img_name = Path(img_path).name.split('.')[0]
    # output = str(output)
    img_shape = img.shape

    if tile_size[0] > img_shape[0] or tile_size[0] < 1 or tile_size[1] > img_shape[1] or tile_size[1] < 1:
        raise ValueError("Please keep tile size bounds within [1, image_size] !")

    if offset[0] > img_shape[0] or offset[0] < 1 or offset[1] > img_shape[1] or offset[1] < 1:
        raise ValueError("Please keep offset bounds within [1, image_size] !")

    for i in range(int(math.ceil(img_shape[0] / (offset[1] * 1.0)))):
        for j in range(int(math.ceil(img_shape[1] / (offset[0] * 1.0)))):
            cropped_img = img[offset[1] * i:min(offset[1] * i + tile_size[1], img_shape[0]),
                              offset[0] * j:min(offset[0] * j + tile_size[0], img_shape[1])]
            # Save the tiles
            cv2.imwrite(str(output) + "/" + str(img_name) + "_" + str(i) + "_" + str(j) + ".png", cropped_img)


def main():
    path = Path(SAMPLE_PATH)
    path_img = path / 'images'
    path_msk = path / 'masks'

    output_dir = Path(CROPS_PATH)
    output_img = output_dir / "images"
    output_msk = output_dir / "masks"

    imgs = get_files(path_img)
    tile_size = (256, 256)
    offset = (256, 256)
    print("Start cropping the images!")
    for img in tqdm(imgs, unit='images', leave=False):
        crop_image(img, output_img, tile_size, offset)

    msks = get_files(path_msk)
    print("Start cropping the masks!")
    for msk in tqdm(msks, unit='masks', leave=False):
        crop_image(msk, output_msk, tile_size, offset)
    print("Done!")


if __name__ == "__main__":
    main()
