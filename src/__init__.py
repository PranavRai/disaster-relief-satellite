from os.path import abspath, dirname, exists, isfile
from os import mkdir, makedirs, getcwd
import logging
from mini_utils import download_models

logging.basicConfig(level=logging.INFO)


ROOT_PATH = dirname(dirname(abspath(__file__)))
DATA_PATH = f'{ROOT_PATH}/data/raw/train'
BASELINE_PATH = f'{ROOT_PATH}/xview2-baseline'

SAMPLE_PATH = f'{ROOT_PATH}/data/sample'
MODELS_PATH = f'{ROOT_PATH}/models'
CROPS_PATH = f'{ROOT_PATH}/data/crops'
CLASSIFICATION_PATH = f'{ROOT_PATH}/data/classification'

makedirs(MODELS_PATH + "/downloaded_full_baseline") if not exists(MODELS_PATH + "/downloaded_full_baseline") else None


makedirs(SAMPLE_PATH + "/images/") if not exists(SAMPLE_PATH + "/images/") else None
mkdir(SAMPLE_PATH + "/labels/") if not exists(SAMPLE_PATH + "/labels/") else None

makedirs(CROPS_PATH + "/images/") if not exists(CROPS_PATH + "/images/") else None
mkdir(CROPS_PATH + "/masks/") if not exists(CROPS_PATH + "/masks/") else None

makedirs(CLASSIFICATION_PATH + "/polygons/") if not exists(CLASSIFICATION_PATH + "/polygons/") else None

model_files = ["downloaded_full_baseline/classification.hdf5", "downloaded_full_baseline/localization.h5",
               "segmentation_res34.pkl", "classification_res34.pkl"]
for model in model_files:
    if not isfile(MODELS_PATH + "/" + model):
        download_models(MODELS_PATH + "/" + model, model)
