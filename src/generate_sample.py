from src import DATA_PATH, SAMPLE_PATH
import os
import random
from shutil import copyfile
import argparse
import sys


def generate(num_samples: int):
    """
    Generate a random sample of the full data using sampling without replacement
    :param num_samples: How many samples to be generated
    :return:
    """

    print(f'Preparing sample of size {num_samples}')

    files = os.listdir(DATA_PATH + "/labels/")
    files = [file for file in files if "_pre_" in file]
    random.seed(42424242.2312)
    random.shuffle(files)
    files = files[:num_samples]

    for folder in ["/images/", "/labels/", "/masks/"]:
        os.makedirs(SAMPLE_PATH + folder) if not os.path.exists(SAMPLE_PATH + folder) else None
        clean_previous(SAMPLE_PATH + folder)

    for folder in ["/images/", "/labels/"]:
        for pre_or_post in ["pre", "post"]:
            for file in files:
                src = DATA_PATH + folder + file
                dest = SAMPLE_PATH + folder + file
                src = src[:-5] + ".png" if folder == "/images/" else src
                dest = dest[:-5] + ".png" if folder == "/images/" else dest
                if pre_or_post == "post":
                    src = src.replace("_pre_disaster", "_post_disaster")
                    dest = dest.replace("_pre_disaster", "_post_disaster")

                copyfile(src, dest)
    print("Done!")


def clean_previous(path):
    if not os.path.exists(path):
        raise FileNotFoundError

    for root, dirs, files in os.walk(path):
        for file in files:
            print(root)
            print(file)
            os.remove(os.path.join(root, file))


def parse_args(args):
    parser = argparse.ArgumentParser(description='Sampling from raw data')
    parser.add_argument('--num',
                        required=False,
                        metavar='1000',
                        default=1000,
                        help="The number of samples")

    args = parser.parse_args(args)
    if not int(args.num):
        raise TypeError("please enter integer number")

    return args


if __name__ == "__main__":
    clean_previous(SAMPLE_PATH)
    args = parse_args(sys.argv[1:])
    num = int(args.num)
    generate(num_samples=num)
