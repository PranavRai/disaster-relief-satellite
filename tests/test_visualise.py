import unittest
import shapely.geometry.polygon as p
import json as json

from src.visualise import get_polygons_from_label


class TestGetPolygonsFromLabel(unittest.TestCase):
    def setUp(self):
        coords = ((532.1863714054954, 165.9824503000504), (548.6019827848852, 224.1613082732768),
                  (446.2458177229872, 264.4761185678668), (440.6934785817455, 249.7503495372735),
                  (425.9677095499687, 253.1300342329752), (420.8981825090282, 232.3691139606),
                  (437.5551999358392, 227.0581808684396), (431.2786426428399, 201.2277335525121),
                  (532.1863714054954, 165.9824503000504))
        polygon = p.Polygon(coords)
        self.data = [('no-damage', polygon)]
        pass

    def test_init(self):
        label = 'test_data/labels/guatemala-volcano_00000000_post_disaster.json'
        result = get_polygons_from_label(label)

        self.assertEqual(result[0], self.data[0])

    def test_file_not_found_error(self):
        label = ''
        with self.assertRaises(FileNotFoundError):
            get_polygons_from_label(label)

    def test_os_error(self):
        label = 12
        with self.assertRaises(OSError):
            get_polygons_from_label(label)

    def test_json_decode_error(self):
        label = 1
        with self.assertRaises(json.decoder.JSONDecodeError):
            get_polygons_from_label(label)


if __name__ == '__main__':
    unittest.main()
