import unittest
import json
import numpy as np
from skimage.io import imread

from src.utilities.mask_polygons import get_dimensions
from src.utilities.mask_polygons import read_json
from src.utilities.mask_polygons import get_feature_info
from src.utilities.mask_polygons import mask_polygons_together
from src.utilities.mask_polygons import save_one_mask


class TestGetDimensions(unittest.TestCase):

    def test_init(self):
        path = 'test_data/images/guatemala-volcano_00000000_post_disaster.png'
        result = get_dimensions(path)

        self.assertEqual(result, (1024, 1024, 3))

    def test_os_error(self):
        path = 123
        with self.assertRaises(OSError):
            get_dimensions(path)

    def test_file_no_found_error(self):
        path = 'abc'
        with self.assertRaises(FileNotFoundError):
            get_dimensions(path)


class TestGetFeatureInfo(unittest.TestCase):

    def setUp(self):
        self.path = 'test_data/labels/guatemala-volcano_00000000_post_disaster.json'
        self.json = json.load(open(self.path))
        self.data = {
            '486b0813-ecd2-4b84-856c-9c0e42156953': np.array([[532, 165],
                                                              [548, 224],
                                                              [446, 264],
                                                              [440, 249],
                                                              [425, 253],
                                                              [420, 232],
                                                              [437, 227],
                                                              [431, 201],
                                                              [532, 165]], np.int32),
            '139cf2c8-ad52-4739-82b5-bb646b215e76': np.array([[810, 743],
                                                              [809, 735],
                                                              [836, 731],
                                                              [846, 772],
                                                              [834, 774],
                                                              [831, 766],
                                                              [794, 773],
                                                              [793, 763],
                                                              [828, 759],
                                                              [826, 740],
                                                              [824, 741],
                                                              [824, 743],
                                                              [820, 743],
                                                              [819, 742],
                                                              [810, 743]], np.int32),
            'd43deb4a-529c-4df4-b666-26dd5b17e040': np.array([[1023, 854],
                                                              [1003, 858],
                                                              [1002, 857],
                                                              [990, 858],
                                                              [990, 857],
                                                              [981, 857],
                                                              [976, 828],
                                                              [1023, 822],
                                                              [1023, 854]], np.int32),
            '563b145d-732d-4eb4-8c77-380519842324': np.array([[1023, 937],
                                                              [1014, 939],
                                                              [1003, 901],
                                                              [1020, 897],
                                                              [1023, 909],
                                                              [1023, 937]], np.int32),
            '56f51b26-d511-461d-bb31-747901a4ea75': np.array([[1023, 988],
                                                              [997, 999],
                                                              [983, 968],
                                                              [971, 972],
                                                              [968, 958],
                                                              [980, 955],
                                                              [964, 907],
                                                              [999, 894],
                                                              [1023, 968],
                                                              [1023, 988]], np.int32),
            '7fd91a32-cf6c-43f2-b059-8c77c92a5693': np.array([[944, 1022],
                                                              [942, 1019],
                                                              [947, 1017],
                                                              [945, 1009],
                                                              [962, 1003],
                                                              [970, 1022],
                                                              [944, 1022]], np.int32),
            'aa0e6190-a04e-427b-bcb1-9a518e4ddba7': np.array([[512, 473],
                                                              [518, 490],
                                                              [513, 490],
                                                              [486, 501],
                                                              [480, 484],
                                                              [512, 473]], np.int32),
            'c9128d2c-3ef9-46a4-b6e1-f9c66ad53a29': np.array([[459, 88],
                                                              [452, 92],
                                                              [446, 82],
                                                              [454, 78],
                                                              [459, 88]], np.int32),
            'a52e5ed2-8e9a-422e-98cd-5e0e6e2cb72c': np.array([[418, 0],
                                                              [406, 4],
                                                              [404, 0],
                                                              [418, 0]], np.int32),
            'ec1c17b7-b24d-4723-9f1c-0e0c7f83eeaa': np.array([[925, 563],
                                                              [929, 581],
                                                              [912, 584],
                                                              [910, 565],
                                                              [925, 563]], np.int32)}

        pass

    def test_read_json(self):
        result = read_json(self.path)

        self.assertEqual(result, self.json)

    def test_get_feature_info(self):
        result = get_feature_info(self.json)

        for array in self.data:
            bool = (self.data[array] == result[array]).all()
            self.assertTrue(bool)

    def test_key_error(self):
        feature = {}
        with self.assertRaises(KeyError):
            get_feature_info(feature)

    def test_type_error(self):
        feature = 'abc'
        with self.assertRaises(TypeError):
            get_feature_info(feature)


class TestMaskPolygonsTogether(unittest.TestCase):

    def setUp(self):
        self.path = 'test_data/labels/guatemala-volcano_00000000_post_disaster.json'
        self.json = json.load(open(self.path))
        pil_img = imread('test_data/masks/guatemala-volcano_00000000_post_disaster.png')
        self.img = np.array(pil_img)
        pass

    def test_init(self):
        size = (1024, 1024, 3)
        polygons = get_feature_info(self.json)
        result = mask_polygons_together(size, polygons)
        bool = (self.img == result).all()
        self.assertTrue(bool)

    def test_type_error_of_first_pram(self):
        size = '1024'
        polygons = get_feature_info(self.json)

        with self.assertRaises(TypeError):
            mask_polygons_together(size, polygons)

    def test_type_error_of_second_pram(self):
        size = (1024, 1024, 3)
        polygons = "abc"

        with self.assertRaises(TypeError):
            mask_polygons_together(size, polygons)


class TestSaveOneMask(unittest.TestCase):

    def setUp(self):
        path = 'test_data/labels/guatemala-volcano_00000000_post_disaster.json'
        label = json.load(open(path))
        size = (1024, 1024, 3)
        polygons = get_feature_info(label)
        self.masks = mask_polygons_together(size, polygons)
        pass

    def test_init(self):
        output_path = 'test_data/masks'
        mask_file_name = 'guatemala-volcano_00000000_post_disaster'
        result = save_one_mask(self.masks, output_path, mask_file_name)

        self.assertTrue(result)

    def test_invalue_directory(self):
        output_path = 'test_data/abc'
        mask_file_name = 'guatemala-volcano_00000000_post_disaster'

        with self.assertRaises(ValueError):
            save_one_mask(self.masks, output_path, mask_file_name)


if __name__ == '__main__':
    unittest.main()
