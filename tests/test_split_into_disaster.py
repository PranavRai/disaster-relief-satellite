import unittest
import os

from src.split_into_disaster import get_pre_files


class TestGetPreFile(unittest.TestCase):

    def test_init(self):
        TESTDATA_DIR = os.path.join(os.path.dirname(__file__), 'test_data')
        files = get_pre_files(TESTDATA_DIR)

        self.assertEqual(files, ["guatemala-volcano_00000000_pre_disaster.png"])

    def test_type_error(self):
        filename = 123
        with self.assertRaises(TypeError):
            get_pre_files(filename)

    def test_file_no_found_error(self):
        filename = 'abc'
        with self.assertRaises(FileNotFoundError):
            get_pre_files(filename)


if __name__ == '__main__':
    unittest.main()
