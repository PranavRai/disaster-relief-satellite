#!/bin/bash
#SBATCH -o _env_make.txt  # send stdout to outfile

ENV_NAME="baseline_gpu"

conda create -n $ENV_NAME python=3.6 -y

echo "###   Env created  ###"
conda activate $ENV_NAME && conda install pip

echo "###################################"
pip install numpy tensorflow-gpu && conda install cupy -y
echo "###################################"
pip install -r ../xview2-baseline/requirements.txt

echo "DONE!"

