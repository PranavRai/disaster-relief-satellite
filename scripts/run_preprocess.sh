#!/bin/bash
#SBATCH -o out.pre.txt  # send stdout to outfile

ENV_NAME="baseline_gpu"
conda create -n $ENV_NAME python=3.6 -y

eval "$(conda shell.bash hook)"
conda activate $ENV_NAME
conda install pip -y
pip install numpy tensorflow-gpu && conda install cupy -y

cd ..
pip install -r xview2-baseline/requirements.txt

python xview2-baseline/utils/split_into_disasters.py --input data/sample/train/ --output results/xBD-data/
python xview2-baseline/utils/mask_polygons.py --input results/xBD-data/ --single-file --border 2
bash xview2-baseline/utils/data_finalize.sh -i results/xBD-data -x xview2-baseline -s .75

