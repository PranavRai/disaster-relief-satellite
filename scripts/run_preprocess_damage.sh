#!/bin/bash
#SBATCH -o out.pre_damage.txt  # send stdout to outfile

ENV_NAME="baseline_gpu"

eval "$(conda shell.bash hook)"
conda activate $ENV_NAME

cd ..

mkdir results/xBD-polygons/
mkdir results/xBD/
mv results/xBD-data/spacenet_gt temp/

cd xview2-baseline/model
python process_data.py --input_dir ../../results/xBD-data --output_dir ../../results/xBD-polygons --output_dir_csv ../../results/xBD
