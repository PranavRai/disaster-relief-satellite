#!/bin/bash
#SBATCH -o out.infer.txt  # send stdout to outfile

ENV_NAME="baseline_gpu"

eval "$(conda shell.bash hook)"
conda activate $ENV_NAME

pre="data/raw/train/images/santa-rosa-wildfire_00000291_pre_disaster.png"
post="data/raw/train/images/santa-rosa-wildfire_00000291_post_disaster.png"
localization_model="models/downloaded_full_baseline/localization.h5"
damage_model="models/downloaded_full_baseline/classification.hdf5"
#localization_model="models/localization/model_iter_98"
#damage_model="models/xBD/baseline_trial-saved-model-02-0.07.hdf5"
output="results/output_image.png"

cd ..
mv temp/spacenet_gt results/xBD-data/
bash xview2-baseline/utils/inference.sh -x xview2-baseline/ -i $pre -p $post -l $localization_model -c $damage_model -o $output -y
