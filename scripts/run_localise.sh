#!/bin/bash
#SBATCH -o out.localise.txt  # send stdout to outfile

ENV_NAME="baseline_gpu"

eval "$(conda shell.bash hook)"
conda activate $ENV_NAME

cd ../xview2-baseline/spacenet/src/models/

python train_model.py ../../../../results/xBD-data/spacenet_gt/dataSet ../../../../results/xBD-data/spacenet_gt/images/ ../../../../results/xBD-data/spacenet_gt/labels -e 15 --batchsize 4

