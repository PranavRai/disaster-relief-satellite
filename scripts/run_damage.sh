#!/bin/bash
#SBATCH -o out.damage.txt  # send stdout to outfile

ENV_NAME="baseline_gpu"

#export PATH=~/miniconda3/bin:$PATH
eval "$(conda shell.bash hook)"
#source ~/miniconda3/bin/activate $ENV_NAME
conda activate $ENV_NAME

cd ../xview2-baseline/model/

python damage_classification.py --train_data ../../results/xBD-polygons --train_csv ../../results/xBD/train.csv --test_data ../../results/xBD-polygons --test_csv ../../results/xBD/test.csv --model_out ../../models/xBD/baseline_trial
# [--model_in ../../models/xBD/baseline_trial/saved-model-01.hdf5]
