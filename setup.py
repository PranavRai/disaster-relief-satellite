from setuptools import setup


with open("README.md", 'r') as r:
    long_description = r.read()

with open("requirements.txt", 'r') as r:
    requirements = r.readlines()

setup(name='disaster-relief',
      version='1.0',
      description='Our code for the XView2 Challenge - Visualisations, Segmentation, and Classification',
      long_description=long_description,
      url='https://gitlab.com/PranavRai/disaster-relief-satellite/',
      author='Pranav Rai and Kehong Deng',
      author_email='pranavrai95@gmail.com, Sevil9955@gmail.com ',
      license='MIT',
      packages=['src', 'xview2-baseline'],
      install_requires=requirements,
      # install_requires=['bar', 'greek'],
      zip_safe=False)
